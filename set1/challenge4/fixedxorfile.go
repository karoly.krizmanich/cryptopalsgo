package main

import (
	"bufio"
	"fmt"
	"math"
	"os"

	fx "krizmanich.hu/fixedxor/xorcipher"
	b64 "krizmanich.hu/hex2base64"
)

func main() {

	readFile, err := os.Open("4.txt")
  
	if err != nil {
			fmt.Println(err)
	}
	defer readFile.Close()
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	i:=0
	bestRank := math.MaxFloat64
	var bestPlainText []byte
	var bestKey byte
	for fileScanner.Scan() {
		i++
		s := fileScanner.Text()
		bytes := b64.HexToBytes(s)
		plainText, key, rank := fx.DecipherFixedXor(bytes)
		if rank < bestRank {
			bestPlainText = []byte(plainText)
			bestKey = byte(key)
			bestRank = rank
		}
	}
	fmt.Printf("plainText: %s, key: %d, rank: %f\n", bestPlainText, bestKey, bestRank)

	/*
		lineWritten := false
		for c := 0; c < 256; c++ {
			outLine := fx.Decipher(bytes, uint8(c))
			out := outLine
//			out := strings.TrimSuffix(outLine, "\n")
			if fx.IsPrintable(out) {
				//rank := fx.Rate(fx.CountFrequencies(out))
				rank := fx.Rate3(out)
				if rank < bestRank {
					if !lineWritten {
					  fmt.Printf("line: %d: %s\n", i, s)
						lineWritten = true
					}
					fmt.Printf("string: %s, rank: %f, key: %c\n", out, rank, c)
					bestRank = rank
				}
			}
		}
	}
	*/

}
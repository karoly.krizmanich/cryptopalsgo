module krizmanich.hu/fixedxorfile

replace krizmanich.hu/hex2base64 => ../challenge1

replace krizmanich.hu/fixedxor => ../challenge3

go 1.19

require (
	krizmanich.hu/fixedxor v0.0.0-00010101000000-000000000000
	krizmanich.hu/hex2base64 v0.0.0-00010101000000-000000000000
)

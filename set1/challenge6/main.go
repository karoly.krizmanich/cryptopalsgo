package main

import (
	"bufio"
	b64 "encoding/base64"
	"fmt"
	"math"
	"os"
	"sort"

	fx "krizmanich.hu/fixedxor/xorcipher"
)

func getHamming(s1 string, s2 string) int {
	dist := 0
	for i:=0; i<len(s1); i++ {
		b := byte(s1[i]) ^ byte(s2[i])
		for ; b > 0; dist++ {
			b = b & (b - 1)
		}
	}
	return dist
}

func hamming_test() {
	var s1 string = "this is a test"
	var s2 string = "wokka wokka!!!"

	fmt.Printf("hamming distance: %d\n", getHamming(s1, s2))
}

func guessKeyLength(cipherText []byte) []int {
	var hammings = make(map[int]float32)
	for keySize := 2; keySize < 40 ; keySize++ {
		block1 := cipherText[0:keySize]
		block2 := cipherText[keySize:2*keySize]
		hammings[keySize] = float32(getHamming(string(block1), string(block2))) / float32(keySize)
	}

	keySizes := make([]int, 0, len(hammings))

	for k := range hammings {
		keySizes = append(keySizes, k)
	}

	sort.SliceStable(keySizes, func(i,j int) bool{
		return hammings[keySizes[i]] < hammings[keySizes[j]]
	})

	return keySizes
}

func split(data []byte, blockSize int) [][]byte {
	var ret [][]byte = make([][]byte, 0)

	blockNo := len(data) / blockSize
	if blockNo * blockSize < len(data) { 
		blockNo+=1 
	}

	for i := 0; i < blockNo; i++ {
		nextI := (i + 1) * blockSize
		if (nextI > len(data)) {
			nextI = len(data)
		}
		ret = append(ret, data[i*blockSize: nextI])
	}
	return ret 
}

func Gsplit[T any](data []T, blockSize int) [][]T {
	var ret [][]T = make([][]T, 0)

	blockNo := len(data) / blockSize
	if blockNo * blockSize < len(data) { 
		blockNo+=1 
	}

	for i := 0; i < blockNo; i++ {
		nextI := (i + 1) * blockSize
		if (nextI > len(data)) {
			nextI = len(data)
		}
		ret = append(ret, data[i*blockSize: nextI])
	}
	return ret 
}


func transpose(blocks [][]byte) [][]byte {
	var ret [][]byte = make([][]byte, 0, len(blocks[0]))

	for i:=0; i<len(blocks); i++ {
		for j:=0; j<len(blocks[i]); j++ {
			if j>=len(ret) {
				ret = append(ret, make([]byte, 0, len(blocks)))
			}
			ret[j] = append(ret[j], blocks[i][j])
		}
	} 

	return ret
}

func Gtranspose[T any](blocks [][]T) [][]T {
	var ret [][]T = make([][]T, 0, len(blocks[0]))

	for i:=0; i<len(blocks); i++ {
		for j:=0; j<len(blocks[i]); j++ {
			if j>=len(ret) {
				ret = append(ret, make([]T, 0, len(blocks)))
			}
			ret[j] = append(ret[j], blocks[i][j])
		}
	} 

	return ret
}


func join(blocks [][]byte) []byte {
	cap := 0
	if len(blocks) > 0 {
		cap = len(blocks)*len(blocks[0])
	}
	var ret []byte = make([]byte, 0, cap)

	for _, b := range blocks {
		ret = append(ret, b...)
	}

	return ret
}

func Gjoin[T any](blocks [][]T) []T {
	cap := 0
	if len(blocks) > 0 {
		cap = len(blocks)*len(blocks[0])
	}
	var ret []T = make([]T, 0, cap)

	for _, b := range blocks {
		ret = append(ret, b...)
	}

	return ret
}


func split_test() {
	test_data := []byte{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
	fmt.Println(split(test_data, 2))
	fmt.Println(split(test_data, 2))
	fmt.Println(transpose(split(test_data, 2)))
	fmt.Println(transpose(split(test_data, 2)))
	fmt.Println(transpose(transpose(split(test_data, 2))))
	fmt.Println(transpose(transpose(split(test_data, 3))))
	fmt.Println(join(split(test_data, 2)))
	fmt.Println(join(split(test_data, 2)))
}

func Gsplit_test() {
	test_data := []byte{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
	fmt.Println(Gsplit[byte](test_data, 2))
	fmt.Println(Gsplit[byte](test_data, 2))
	fmt.Println(Gtranspose[byte](Gsplit[byte](test_data, 2)))
	fmt.Println(Gtranspose[byte](Gsplit[byte](test_data, 2)))
	fmt.Println(Gtranspose[byte](Gtranspose[byte](Gsplit[byte](test_data, 2))))
	fmt.Println(Gtranspose[byte](Gtranspose[byte](Gsplit[byte](test_data, 3))))
	fmt.Println(Gjoin[byte](Gsplit[byte](test_data, 2)))
	fmt.Println(Gjoin[byte](Gsplit[byte](test_data, 2)))
}


//  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
func transposeBlocks(text []byte, blockSize int) [][]byte {
	var chunks [][]byte

	for b:=0; b < blockSize; b++ {
		chunks = append(chunks, make([]byte, 0, (len(text) / blockSize) + 1))
	}

	for i:=0; i < len(text); i++ {
		blockIndex := i % blockSize
		chunks[blockIndex] = append(chunks[blockIndex], text[i])
	}
	
	return chunks
}

func transposeBlocks_test() {
	test_data := []byte{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}

	for i:=1; i<5; i++ {	
		test_chunks := transposeBlocks(test_data, i)
		fmt.Println(test_chunks)
	}
}

func retransposeBlocks(blocks [][]byte) []byte {
	ret := make([]byte, 0, len(blocks[0])*len(blocks))

	for i:=0; i < len(blocks[0]); i++ {
		for _, block := range blocks {
			if i < len(block) {
				ret = append(ret, block[i])
			}
		}
	}
	return ret
}

func DecipherXor() {

	readFile, err := os.Open("6.txt")
  
	if err != nil {
			fmt.Println(err)
			return
	}
	defer readFile.Close()
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	cipherText := make([]byte, 0)
	for fileScanner.Scan() {
		b64Line := fileScanner.Text()
		cipherLine, err := b64.StdEncoding.DecodeString(b64Line)
		if err != nil {
			fmt.Println("error:", err)
			return
		}
		cipherText = append(cipherText, cipherLine...)
	}
	keyLengths := guessKeyLength(cipherText)
	var bestPlainText []byte
	var bestRank float64
	bestRank = math.MaxFloat64

	for i:=0; i<len(keyLengths); i++ {
		keySize := keyLengths[i]

		transposedBlocks := transpose(split(cipherText, keySize))

		plainTextBlocks := make([][]byte, 0, keySize)
		for _, block := range transposedBlocks {
			plainTextBlock, _, _ := fx.DecipherFixedXor(block)
			plainTextBlocks = append(plainTextBlocks, plainTextBlock)
		}

		plainText := join(transpose(plainTextBlocks))
		rank := fx.Rate3(string(plainText))

		if rank < bestRank {
			bestRank = rank
			bestPlainText = plainText
		}
	}
	fmt.Println(string(bestPlainText))
}

func main() {
	split_test()
	Gsplit_test()
	//DecipherXor()
}
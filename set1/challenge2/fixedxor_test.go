package fixedxor

import (
	"bytes"
	"testing"

	b64 "krizmanich.hu/hex2base64"
)

func TestFixedXor(t *testing.T) {
	in1 := "1c0111001f010100061a024b53535009181c"
	in2 := "686974207468652062756c6c277320657965"
	exp := "746865206b696420646f6e277420706c6179"

	in1Bytes := b64.HexToBytes(in1)
	in2Bytes := b64.HexToBytes(in2)
	expBytes := b64.HexToBytes(exp)

	res, _ := FixedXor(in1Bytes, in2Bytes)

	if !bytes.Equal(res, expBytes) {
		t.Fatalf(`Didn't match %q`, res)

	}

}
package fixedxor

import "errors"

func FixedXor(buf1 []uint8, buf2 []uint8) ([]uint8, error) {
	if len(buf1) != len(buf2) {
		return nil, errors.New("error")
	}

	length := len(buf1)
	res := make([]uint8, length, length)
	for i := 0; i < length; i++ {
		res[i] = buf1[i] ^ buf2[i]
	}
	return res, nil
}

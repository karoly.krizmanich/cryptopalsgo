module krizmanich.hu/fixedxor

go 1.19

replace krizmanich.hu/hex2base64 => ../challenge1

require krizmanich.hu/hex2base64 v0.0.0-00010101000000-000000000000

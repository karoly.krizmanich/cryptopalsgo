package main

import (
	"encoding/hex"
	"fmt"
)

func main() {
	var plaintext string = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
	key := "ICE"
	ciphertext := make([]byte, len(plaintext))

	for i, c := range plaintext {
		ciphertext[i] = byte(c) ^ key[i%3]
	}
	fmt.Println(hex.EncodeToString(ciphertext))

}
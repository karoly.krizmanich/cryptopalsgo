package xorcipher

import (
	"math"
	"strings"
)

var letterPercent = [26]float32{8.34, 1.54, 2.73, 4.14, 12.60, 2.03, 1.92, 6.11, 6.71, 0.23, 0.87, 4.24, 2.53, 6.80, 7.70, 1.66, 0.09, 5.68, 6.11, 9.37, 2.85, 1.06, 2.34, 0.20, 2.04, 0.06}
var spacePercent = float32(19.18182)
var letterPercent2 = map[byte]float32{
	'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339, 'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881, 'g': 0.0158610,
	'h': 0.0492888, 'i': 0.0558094, 'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490, 'm': 0.0202124, 'n': 0.0564513,
	'o': 0.0596302, 'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563, 's': 0.0515760, 't': 0.0729357, 'u': 0.0225134,
	'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692, 'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182}

func Rate(freq []float32) float64 {
	var rank float64 = 0
	for i:=0; i<26; i++ {
		rank += math.Abs(float64(freq[i] - (letterPercent[0]/100.0)))
	}
	return rank
}

func Rate2(s string) float64 {
	var rank float64 = 0
	uppers := strings.ToUpper(s)

	for _, c := range uppers {
		if (c >= 'A') && (c <= 'Z') {
			rank += float64(letterPercent[c-'A'])
		}
		if c == ' ' {
			rank += float64(spacePercent)
		}
	}
	return -1*rank
}

func Rate3(s string) float64 {
	var rank float64 = 0
	lowers := []byte(strings.ToLower(s))
	
	for _, c := range lowers {
		p, ok := letterPercent2[c]
		if ok {
			rank += float64(p)
		}
	}
	return -1*rank
	
}

func CountFrequencies(s string) []float32 {
	res := make([]float32, 26, 26)
	var sum float32 = 0

	uppers := strings.ToUpper(s)

	for i:=0; i<len(s); i++ {
		if uppers[i] >= 'A' && uppers[i] <= 'Z' {
			res[uppers[i] - 'A'] += 1
			sum++
		}
	}

	if sum > 0 {
		for i:=0; i<26; i++ {
			res[i] /= sum
		}
	}

	//fmt.Printf("string: %s, freq: %v\n", s, res)

	return res
}

func IsPrintable(s string) bool {
	for i := 0; i < len(s); i++ {
			if !(((s[i] >= 32) && (s[i] < 127)) || s[i] == 10)   {
					return false
			}
	}
	return true
}

func Decipher(in []uint8, c byte) string {
	out := make([]uint8, len(in), len(in))
	for i:=0; i<len(in); i++ {
		out[i] = in[i] ^ c
	}
	return string(out)
}

func DecipherFixedXor(cipherText []byte) ([]byte, byte, float64) {
	var bestPlainText []byte
	var bestKey byte
	bestRank := math.MaxFloat64
	for key := 0; key <= 255; key++ {
		out := Decipher(cipherText, byte(key))
		if IsPrintable(out) {
			rank := Rate3(out)
			if rank < bestRank {
				bestPlainText = []byte(out)
				bestKey = byte(key)
				bestRank = rank
			}
		}
	}
	return bestPlainText, bestKey, bestRank
}


package main

import (
	"fmt"

	xc "krizmanich.hu/fixedxor/xorcipher"

	b64 "krizmanich.hu/hex2base64"
)


func main() {
	s := "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

	cipherText := b64.HexToBytes(s)

	plainText, key, rank := xc.DecipherFixedXor(cipherText)

	fmt.Printf("plainText: %s, key: %d, rank: %f\n", plainText, key, rank)
}
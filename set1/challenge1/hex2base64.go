package challenge1

import (
	"errors"
	"strconv"
)

func HexToBytes(s string) []uint8 {
	res := make([]uint8 , 0, len(s) / 2)

	for i := 0; i<len(s); i+=2 {
		//fmt.Printf("%s", s[i:i+2])
		b, _ := strconv.ParseUint(s[i:i+2], 16, 8)
		res = append(res, uint8(b))
		//fmt.Printf("(%x)|", b)
	}
	return res
}

func sextettobase64(index uint8) (uint8, error) {
	var ret uint8
	var err error

	ret = 0
	err = nil

	if (index >= 0) && (index <= 25) {
		ret = ('A' + index)
	} else if (index >= 26) && (index <= 51) {
		ret ='a' + index - 26
	} else if (index >= 52)  && (index <= 61) {
		ret = '0' + index - 52
  } else if (index == 62)   {
		ret = '+'
	} else if (index == 63)   {
		ret = '/'
	} else {
		err = errors.New("error")
	}
	//fmt.Printf("%d:%c\n", index, ret)
	return ret, err
}

func encodeblock(bytes []uint8, len int) []uint8 {
	var temp [4]uint8

	s0 := bytes[0] >> 2
	temp[0], _ = sextettobase64(s0)

	s1 := bytes[0] & 0x03 << 4 | bytes[1] & 0xF0 >> 4
	temp[1], _ = sextettobase64(s1)

	if len >= 2 {
		s2 := bytes[1] & 0x0F << 2 | bytes[2] & 0xC0 >> 6
		temp[2], _ = sextettobase64(s2)
	} else {
		temp[2]= '='
	}

	if len >= 3 {
		s3 := bytes[2] & 0x3F
		temp[3], _ = sextettobase64(s3)
	}	else {
		temp[3] = '='
	}

	return temp[:]
}

func bytestobase64(bytes []uint8) string {
	blockNo := len(bytes) / 3
	remainder := len(bytes) - blockNo*3 

	base64 := make([]uint8, 0, (blockNo+1)*4)
	for i := 0; i < blockNo; i++ {
		base64 = append(base64, encodeblock(bytes[i*3:i*3+3], 3)...)
	}
	if remainder > 0 {
		block := [3]uint8 {0,0,0}
		for i:=0; i<remainder; i++ {
			block[i] = bytes[blockNo*3 + i]
		}
		base64 = append(base64, encodeblock(block[:], remainder)...)
	}

	return string(base64)
}


package challenge1

import (
	"testing"
)

func TestBase64Aligned(t *testing.T) {
	var s string
	s = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	exp := "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
	bytes := HexToBytes(s)
	res := bytestobase64(bytes)

	if exp != res {
		t.Fatalf(`Didn't match %q`, res)
	}
}

func TestBase64NotAligned(t *testing.T) {
	var s string
	s = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d6d"
	exp := "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29tbQ=="
	bytes := HexToBytes(s)
	res := bytestobase64(bytes)

	if exp != res {
		t.Fatalf(`Didn't match %q`, res)
	}
}